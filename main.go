package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	// bytes, fileSize := loadFile("data.bin")
	_, fileSize := loadFile("data.bin")

	fmt.Println(fileSize)
	// fmt.Printf("%s", hex.Dump(bytes))

	parts := int(fileSize / 32)
	restBytes := fileSize % 32

	fmt.Println("Parts:", parts, "\tRestbytes:", restBytes)

}

func loadFile(filePath string) ([]byte, int) {
	file, err := os.Open(filePath)

	if err != nil {
		fmt.Println(err)
		panic(err)
	}

	defer file.Close()

	info, err := file.Stat()
	if err != nil {
		panic(err)
	}

	var size = info.Size()
	bytes := make([]byte, size)

	buffer := bufio.NewReader(file)
	_, err = buffer.Read(bytes)

	return bytes, len(bytes)
}
